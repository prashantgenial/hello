H2 , JPA , RestRepository, Web,Swagger, todo application, Docker image

http://localhost:8080/todo [POST,GET]
http://localhost:8080/employee [GET]
http://localhost:8080/swagger-ui.html [in browser]

sample test case written, run on the test case
Profiles
pass profile from command line
pass port from command line

how to pass port from kubernetes to service

Create a Hello World Spring Boot application
initialize git repo
 - git init
 - git add .
 - git commit -m "test"
 
Create a Docker image of the application : added a dockerfile
	- dynamic port for springboot application
	- pass port to docker image

create and Push the Docker image to a Docker registry
C:\java-developper-softwares\libs\apache-maven-3.5.3\conf\settings.xml

	<server>
		<id>docker.io</id>
		<username>prashantgenial</username>
		<password>.....</password>
	</server>
	
	- mvn install dockerfile:build -DpushImageTag
	- mvn dockerfile:push
	- mvn dockerfile:push -Ddockerfile.useMavenSettingsForAuth=true
	
Install Minikube on Windows
Deploy the application to Minikube
Update the application

