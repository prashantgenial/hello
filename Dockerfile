FROM openjdk:8
ARG JAR_FILE
ADD ${JAR_FILE} Hello-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "Hello-0.0.1-SNAPSHOT.jar"]