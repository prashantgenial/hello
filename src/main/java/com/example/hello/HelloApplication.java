package com.example.hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class HelloApplication implements CommandLineRunner {

	@Autowired MyHello myhello ;
	
	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(HelloApplication.class, args);
		/*String[] beanDefinitionNames = ctx.getBeanDefinitionNames();
		for(String bean: beanDefinitionNames) {
			System.out.println(bean);
		}*/
	}

	@Override
	public void run(String... args) throws Exception {
		
		myhello.sayHello();
	}
}
