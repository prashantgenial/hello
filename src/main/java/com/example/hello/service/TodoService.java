package com.example.hello.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.hello.bean.Todo;

@Service
public class TodoService {

	private ArrayList<Todo> todos = new ArrayList<>();
	
	/*{
		todos.add(new Todo(1, "Prashant", "hellow", new Date(), true));
		todos.add(new Todo(2, "Prashant", "hello sdfsd", new Date(), true));
		todos.add(new Todo(3, "Rahul", "hellow", new Date(), true));
		todos.add(new Todo(4, "Amit", "hellow", new Date(), true));
	}*/
	
	public List<Todo> getTodosByUser(String user){
		List<Todo> filteredList = new ArrayList<Todo>();
		for(Todo todo : todos) {
			if(todo.getUser().equalsIgnoreCase(user)) {
				filteredList.add(todo);
			}
		}
		return filteredList;
	}
	
	public boolean addTodo(Todo todo) {
		return todos.add(todo);
		
	}
}
