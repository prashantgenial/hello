package com.example.hello;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MyHello {

	@Value("${spring.application.name}")
	String message;
	
	public void sayHello() {
		System.out.println("Saying Hello......");
		System.out.println(message);
	}
}

