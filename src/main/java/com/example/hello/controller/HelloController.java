package com.example.hello.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.hello.bean.Employee;

@RestController
public class HelloController {
	
	@GetMapping("/employee")
	public Employee getEmployee() {
		Employee e = new Employee();
		e.setAge(10);
		e.setEmpId("111");
		e.setName("prashant");
		
		return e ;
	}
}
