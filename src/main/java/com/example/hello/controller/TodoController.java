package com.example.hello.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.hello.bean.Todo;
import com.example.hello.service.TodoService;

@RestController
public class TodoController {

	@Autowired
	TodoService todoService ;
	
	@GetMapping("/todo/{user}")
	public List<Todo> getTodosByUser(@PathVariable  String user){
		return todoService.getTodosByUser(user);
	}
	
	@PostMapping("/todo")
	public boolean getTodosByUser(@RequestBody Todo todo){
		return todoService.addTodo(todo);
		
		/*ServletUriComponentsBuilder.fromCurrentRequest().path("/id").buildAndExpand(todo.getId())
		.toUri();*/
		
	}
}
