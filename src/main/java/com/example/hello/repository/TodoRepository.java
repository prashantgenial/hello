package com.example.hello.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.example.hello.bean.Todo;

@RepositoryRestResource
public interface TodoRepository extends JpaRepository<Todo, Integer>  {

}
